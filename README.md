## Sonifex Radcap PCIe V4L2 Driver for Linux 4.18+

### Loading the module

Under ./src run:

```
$ make
$ sudo modprobe snd_pcm
$ sudo modprobe videodev
$ sudo insmod radcap_pcie.ko
```

### Unloading the module

```
$ sudo rmmod -f radcap_pcie
```
