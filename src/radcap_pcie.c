// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (c) 2020 Jorge Maidana <jorgem.linux@gmail.com>
 *
 * This driver is a derivative of:
 *
 * Philips SAA7134 driver
 * Copyright (c) 2001-03 Gerd Knorr <kraxel@bytesex.org> [SuSE Labs]
 *
 * Radcap driver for Linux
 * Copyright (c) 2012 Jeff Pages <jeff@sonifex.com.au>
 * Copyright (c) 2012 Kevin Dawson
 *
 * v4l2-pci-skeleton.c
 * Copyright (c) 2014 Cisco Systems, Inc. and/or its affiliates. All rights reserved.
 */

/*
 * Only one V4L2 device node is created per card, /dev/radioX, with a
 * maximum of 32 simultaneous Tuners and ALSA Subdevices.
 */

#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/io-64-nonatomic-lo-hi.h>
#include <linux/iopoll.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/version.h>
#include <linux/videodev2.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-dev.h>
#include <media/v4l2-device.h>
#include <media/v4l2-event.h>
#include <media/v4l2-ioctl.h>
#include <sound/control.h>
#include <sound/core.h>
#include <sound/initval.h>
#include <sound/pcm.h>
#include <sound/tlv.h>

MODULE_AUTHOR("Jorge Maidana <jorgem.linux@gmail.com>");
MODULE_DESCRIPTION("Sonifex Radcap PCIe V4L2 driver");
MODULE_VERSION("1.3.0");
MODULE_LICENSE("GPL");

#define RADCAP_PCIE_DRV_NAME		"Radcap PCIe"
#define RADCAP_PCIE_MAX_CARDS		SNDRV_CARDS

static int radcap_pcie_devcount;

/* Unlock more nodes (Tuner + ALSA Subdevice) */
static char *key[RADCAP_PCIE_MAX_CARDS];
static unsigned int key_count;
module_param_array(key, charp, &key_count, 0);
MODULE_PARM_DESC(key, "'Card ID:KEY' string, comma separated for each card");

static int nodes[RADCAP_PCIE_MAX_CARDS];
module_param_array(nodes, int, NULL, 0);
MODULE_PARM_DESC(nodes, "Set the number of active nodes, comma separated for each card");

/* Red Hat */
#ifndef RHEL_RELEASE_CODE
#define RHEL_RELEASE_CODE		0
#endif

#ifndef RHEL_RELEASE_VERSION
#define RHEL_RELEASE_VERSION(a,b)	(((a) << 8) + (b))
#endif

#define RADCAP_PCIE_MIN_NODES		6
#define RADCAP_PCIE_MAX_NODES		32

/* ALSA */
#define RADCAP_PCIE_BUFFER_SIZE		SZ_128K
#define RADCAP_PCIE_PERIODS_MIN		2
#define RADCAP_PCIE_PERIOD_BYTES_MIN	64
#define RADCAP_PCIE_PERIODS_MAX		(RADCAP_PCIE_BUFFER_SIZE / RADCAP_PCIE_PERIOD_BYTES_MIN)
#define RADCAP_PCIE_PERIOD_BYTES_MAX	(RADCAP_PCIE_BUFFER_SIZE / RADCAP_PCIE_PERIODS_MIN)

/* Helpers */
#define radcap_pcie_am_freq(f)		(((f) * 20480 / 441) & 0xffff)
#define radcap_pcie_fm_freq(f)		((((f) - 97920) * 128 / 51) & 0xffff)

#define radcap_pcie_pr_info(fmt, ...)	pr_info(KBUILD_MODNAME " %s: " fmt,		\
						pci_name(dev->pdev), ##__VA_ARGS__)
#define radcap_pcie_pr_err(fmt, ...)	pr_err(KBUILD_MODNAME " %s: " fmt,		\
						pci_name(dev->pdev), ##__VA_ARGS__)

/* Reserve 16 controls for this driver */
#ifndef V4L2_CID_USER_RADCAP_PCIE_BASE
#define V4L2_CID_USER_RADCAP_PCIE_BASE	(V4L2_CID_USER_BASE + 0x1f00)
#endif

enum {
	V4L2_CID_RADCAP_PCIE_NARROW = (V4L2_CID_USER_RADCAP_PCIE_BASE + 0),
	V4L2_CID_RADCAP_PCIE_DUAL,
};

struct radcap_pcie_card {
	u32				reg[12];
	u32				val[12];
	const	char			*band;
	u32				nodes;
};

struct radcap_pcie_ctrl {
	u32				freq[RADCAP_PCIE_MAX_NODES];
	u32				fm_audmode_mask;
	u32				fm_audmode_set_mask;
	s32				fm_deemph;
	bool				narrow;
	bool				dual;
	u8				reserved[18];
};

struct radcap_pcie_snd {
	struct	mutex			snd_mlock;
	struct	radcap_pcie_dev		*dev;
	struct	snd_pcm_substream	*pcm_substream[RADCAP_PCIE_MAX_NODES];
	u16				mixer_volume[RADCAP_PCIE_MAX_NODES][2];
	u32				pcm_substream_mask;
};

struct radcap_pcie_pcm {
	struct	radcap_pcie_dev		*dev;
};

struct radcap_pcie_dev {
	spinlock_t			slock;
	struct	radcap_pcie_card	card;
	char				name[32];
	u64				fpga_id;
	u32				board;
	u32				nodes;
	s32				nr;

	/* PCI */
	struct	pci_dev			*pdev;
	u64	__iomem			*ctrl_pcm;
	u32	__iomem			*ctrl_radio;
	u32				irq;
	int				error;

	/* V4L2 */
	struct	v4l2_device		v4l2_dev;
	struct	v4l2_ctrl_handler	ctrl_handler;
	struct	video_device		rdev;
	struct	mutex			rdev_mlock;
	struct	radcap_pcie_ctrl	rctrl;

	/* ALSA */
	struct	snd_card		*snd_card;
	struct	radcap_pcie_snd		*snd_chip;
	bool				snd_ready;
};

enum {
	RADCAP_TUNER_AM,
	RADCAP_TUNER_FM,
};

static const struct v4l2_tuner radcap_pcie_tuners[] = {
	[RADCAP_TUNER_AM] = {
		.type		= V4L2_TUNER_RADIO,
		.capability	= V4L2_TUNER_CAP_LOW,
		.audmode	= V4L2_TUNER_MODE_MONO,
		.rxsubchans	= V4L2_TUNER_SUB_MONO,
		.rangelow	= (520 * 16),
		.rangehigh	= (1710 * 16),
	},
	[RADCAP_TUNER_FM] = {
		.type		= V4L2_TUNER_RADIO,
		.capability	= (V4L2_TUNER_CAP_LOW | V4L2_TUNER_CAP_STEREO), /* Stereo = auto */
		.audmode	= V4L2_TUNER_MODE_STEREO,
		.rxsubchans	= V4L2_TUNER_SUB_STEREO,
		.rangelow	= (87500 * 16),
		.rangehigh	= (108000 * 16),
	},
	{ }
};

enum {
	RADCAP_PCIE_AM,
	RADCAP_PCIE_FM,
};

static const struct radcap_pcie_card radcap_pcie_cards[] = {
	[RADCAP_PCIE_AM] = {
		.band	= "AM",
		.reg	= { 0x0040, 0x0001, 0x0000, 0x0005, 0x0004, 0x0000,
			    0x0001, 0x0060, 0x0060, 0x0000, 0x0000, 0x0000 },
		.val	= { 0x00000000, 0x00000003, 0x00000011, 0x00000015,
			    0x00000000, 0x00000008, 0x00000000, 0x00000000,
			    0x00000000, 0x00000000, 0x00000000, 0x00000000 },
	},
	[RADCAP_PCIE_FM] = {
		.band	= "FM",
		.reg	= { 0x0020, 0x0001, 0x0000, 0x0002, 0x0006, 0x0000,
			    0x0001, 0x0040, 0x0010, 0x0006, 0x0007, 0x0070 },
		.val	= { 0x00000000, 0x00000003, 0x00000000, 0xffffffff,
			    0x00000000, 0x00000004, 0x00000030, 0x00000070,
			    0xffffffff, 0x00000000, 0x00000000, 0x00000000 },
	},
	{ }
};

static const struct snd_pcm_hardware radcap_pcie_pcm_hw = {
	.info			= (SNDRV_PCM_INFO_BLOCK_TRANSFER | SNDRV_PCM_INFO_INTERLEAVED |
				   SNDRV_PCM_INFO_MMAP | SNDRV_PCM_INFO_MMAP_VALID |
				   SNDRV_PCM_INFO_SYNC_START),
	.formats		= SNDRV_PCM_FMTBIT_S16_LE,
	.rates			= (SNDRV_PCM_RATE_22050 | SNDRV_PCM_RATE_48000),
	.rate_min		= 22050,
	.rate_max		= 48000,
	.channels_min		= 2,
	.channels_max		= 2,
	.buffer_bytes_max	= RADCAP_PCIE_BUFFER_SIZE,
	.period_bytes_min	= RADCAP_PCIE_PERIOD_BYTES_MIN,
	.period_bytes_max	= RADCAP_PCIE_PERIOD_BYTES_MAX,
	.periods_min		= RADCAP_PCIE_PERIODS_MIN,
	.periods_max		= RADCAP_PCIE_PERIODS_MAX,
};

static __always_inline u32 radcap_pcie_ioread(struct radcap_pcie_dev *dev, u32 reg)
{
	return ioread32(dev->ctrl_radio + reg);
}

static __always_inline void radcap_pcie_iowrite(struct radcap_pcie_dev *dev, u32 reg, u32 val)
{
	iowrite32(val, dev->ctrl_radio + reg);
}

static void radcap_pcie_pcm_runtime_free(struct snd_pcm_runtime *runtime)
{
	struct radcap_pcie_pcm *pcm = runtime->private_data;

	kfree(pcm);
}

static int radcap_pcie_pcm_open(struct snd_pcm_substream *substream)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct radcap_pcie_snd *chip = snd_pcm_substream_chip(substream);
	struct radcap_pcie_pcm *pcm;
	struct radcap_pcie_dev *dev;
	u32 source = substream->number;
	unsigned long flags;

	if (!chip)
		return -ENODEV;

	pcm = kzalloc(sizeof(*pcm), GFP_KERNEL);
	if (!pcm)
		return -ENOMEM;

	dev = chip->dev;
	pcm->dev = dev;
	runtime->private_data = pcm;
	runtime->private_free = radcap_pcie_pcm_runtime_free;
	runtime->hw = radcap_pcie_pcm_hw;

	switch (dev->board) {
	case RADCAP_PCIE_AM:
		runtime->hw.rates = SNDRV_PCM_RATE_22050;
		runtime->hw.rate_max = 22050;
		break;
	case RADCAP_PCIE_FM:
		runtime->hw.rates = SNDRV_PCM_RATE_48000;
		runtime->hw.rate_min = 48000;
		break;
	}

	spin_lock_irqsave(&dev->slock, flags);
	chip->pcm_substream[source] = substream;
	spin_unlock_irqrestore(&dev->slock, flags);
	return 0;
}

static int radcap_pcie_pcm_close(struct snd_pcm_substream *substream)
{
	struct radcap_pcie_snd *chip = snd_pcm_substream_chip(substream);
	struct radcap_pcie_dev *dev = chip->dev;
	u32 source = substream->number;
	unsigned long flags;

	spin_lock_irqsave(&dev->slock, flags);
	chip->pcm_substream[source] = NULL;
	spin_unlock_irqrestore(&dev->slock, flags);
	return 0;
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 5, 0) && RHEL_RELEASE_CODE < RHEL_RELEASE_VERSION(8,3)
static int radcap_pcie_pcm_hw_params(struct snd_pcm_substream *substream,
				     struct snd_pcm_hw_params *hw_params)
{
	int ret;

	ret = snd_pcm_lib_malloc_pages(substream, params_buffer_bytes(hw_params));
	if (ret < 0)
		return ret;
	return 0;
}

static int radcap_pcie_pcm_hw_free(struct snd_pcm_substream *substream)
{
	return snd_pcm_lib_free_pages(substream);
}
#endif

static int radcap_pcie_pcm_prepare(struct snd_pcm_substream *substream)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct radcap_pcie_snd *chip = snd_pcm_substream_chip(substream);
	struct radcap_pcie_dev *dev = chip->dev;
	u32 page, source = substream->number;
	size_t size = frames_to_bytes(runtime, runtime->buffer_size);

	if (dev->error)
		return dev->error;

	mutex_lock(&chip->snd_mlock);
	radcap_pcie_iowrite(dev, dev->card.reg[7] + source, chip->mixer_volume[source][0] |
			   ((u32)chip->mixer_volume[source][1] << 16));
	mutex_unlock(&chip->snd_mlock);

	/* PCM DMA table */
	for (page = 0; page * 4096 < size; page++) {
		u64 val = snd_pcm_sgbuf_get_addr(substream, page * 4096) & 0xfffffffffffff000ULL;

		if (size - page * 4096 > 4096)
			val |= 0x400; /* New page */
		else
			val |= ((size - page * 4096) & 0xffc) >> 2; /* Last page */
		lo_hi_writeq(val, dev->ctrl_pcm + source * 64 + page);
	}
	return 0;
}

static int radcap_pcie_pcm_trigger(struct snd_pcm_substream *substream, int cmd)
{
	struct radcap_pcie_snd *chip = snd_pcm_substream_chip(substream);
	struct radcap_pcie_dev *dev = chip->dev;
	u32 source = substream->number;
	unsigned long flags;
	int ret = 0;

	spin_lock_irqsave(&dev->slock, flags);
	switch (cmd) {
	case SNDRV_PCM_TRIGGER_START:
		if (dev->error) {
			ret = dev->error;
			break;
		}
		chip->pcm_substream_mask |= BIT(source);
		radcap_pcie_iowrite(dev, dev->card.reg[0] + source, dev->card.val[1]);
		break;
	case SNDRV_PCM_TRIGGER_STOP:
		chip->pcm_substream_mask &= ~(BIT(source));
		if (dev->error) {
			ret = dev->error;
			break;
		}
		radcap_pcie_iowrite(dev, dev->card.reg[0] + source, dev->card.val[0]);
		break;
	default:
		ret = -EINVAL;
		break;
	}
	spin_unlock_irqrestore(&dev->slock, flags);
	return ret;
}

static snd_pcm_uframes_t radcap_pcie_pcm_pointer(struct snd_pcm_substream *substream)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct radcap_pcie_pcm *pcm = runtime->private_data;
	struct radcap_pcie_dev *dev = pcm->dev;
	u32 source = substream->number;
	snd_pcm_uframes_t pointer;

	if (dev->error)
		return SNDRV_PCM_POS_XRUN;

	pointer = radcap_pcie_ioread(dev, dev->card.reg[0] + source);
	if (pointer == 0xffffffff) {
		dev->error = -ENODEV; /* Device disconnected */
		return SNDRV_PCM_POS_XRUN;
	}

	pointer = bytes_to_frames(runtime, pointer);
	if (pointer >= runtime->buffer_size)
		pointer = 0;
	return pointer;
}

static const struct snd_pcm_ops radcap_pcie_pcm_ops = {
	.open		= radcap_pcie_pcm_open,
	.close		= radcap_pcie_pcm_close,
#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 5, 0) && RHEL_RELEASE_CODE < RHEL_RELEASE_VERSION(8,3)
	.ioctl		= snd_pcm_lib_ioctl,
	.hw_params	= radcap_pcie_pcm_hw_params,
	.hw_free	= radcap_pcie_pcm_hw_free,
	.page		= snd_pcm_sgbuf_ops_page,
#endif
	.prepare	= radcap_pcie_pcm_prepare,
	.trigger	= radcap_pcie_pcm_trigger,
	.pointer	= radcap_pcie_pcm_pointer,
};

static int radcap_pcie_pcm_create(struct radcap_pcie_snd *chip, struct snd_card *card)
{
	struct snd_pcm *pcm;
	struct radcap_pcie_dev *dev = chip->dev;
	int ret;

	ret = snd_pcm_new(card, RADCAP_PCIE_DRV_NAME, 0, 0, (int)dev->nodes, &pcm);
	if (ret < 0)
		return ret;

	snd_pcm_set_ops(pcm, SNDRV_PCM_STREAM_CAPTURE, &radcap_pcie_pcm_ops);
	pcm->private_data = chip;
	pcm->info_flags = 0;
	strscpy(pcm->name, RADCAP_PCIE_DRV_NAME, sizeof(pcm->name));
#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 5, 0) && RHEL_RELEASE_CODE < RHEL_RELEASE_VERSION(8,3)
	snd_pcm_lib_preallocate_pages_for_all(pcm, SNDRV_DMA_TYPE_DEV_SG, &dev->pdev->dev,
					      RADCAP_PCIE_BUFFER_SIZE, RADCAP_PCIE_BUFFER_SIZE);
#else
	snd_pcm_set_managed_buffer_all(pcm, SNDRV_DMA_TYPE_DEV_SG, &dev->pdev->dev,
				       RADCAP_PCIE_BUFFER_SIZE, RADCAP_PCIE_BUFFER_SIZE);
#endif
	return 0;
}

static int radcap_pcie_mixer_info(struct snd_kcontrol __always_unused *kcontrol,
				  struct snd_ctl_elem_info *uinfo)
{
	uinfo->value.integer.min = 0x0000;
	uinfo->value.integer.max = 0xffff;
	uinfo->type = SNDRV_CTL_ELEM_TYPE_INTEGER;
	uinfo->count = 2;
	return 0;
}

static int radcap_pcie_mixer_get(struct snd_kcontrol *kcontrol, struct snd_ctl_elem_value *ucontrol)
{
	struct radcap_pcie_snd *chip = snd_kcontrol_chip(kcontrol);
	u32 source = kcontrol->id.subdevice;

	mutex_lock(&chip->snd_mlock);
	ucontrol->value.integer.value[0] = chip->mixer_volume[source][0];
	ucontrol->value.integer.value[1] = chip->mixer_volume[source][1];
	mutex_unlock(&chip->snd_mlock);
	return 0;
}

static int radcap_pcie_mixer_put(struct snd_kcontrol *kcontrol, struct snd_ctl_elem_value *ucontrol)
{
	struct radcap_pcie_snd *chip = snd_kcontrol_chip(kcontrol);
	struct radcap_pcie_dev *dev = chip->dev;
	u32 source = kcontrol->id.subdevice;
	u16 left, right;
	int change = 0;

	if (dev->error)
		return dev->error;

	left  = (u16)clamp_t(long, ucontrol->value.integer.value[0], 0x0000, 0xffff);
	right = (u16)clamp_t(long, ucontrol->value.integer.value[1], 0x0000, 0xffff);

	mutex_lock(&chip->snd_mlock);
	if ((chip->mixer_volume[source][0] != left) || (chip->mixer_volume[source][1] != right)) {
		chip->mixer_volume[source][0] = left;
		chip->mixer_volume[source][1] = right;
		radcap_pcie_iowrite(dev, dev->card.reg[7] + source, chip->mixer_volume[source][0] |
				   ((u32)chip->mixer_volume[source][1] << 16));
		change = 1;
	}
	mutex_unlock(&chip->snd_mlock);
	return change;
}

static const DECLARE_TLV_DB_LINEAR(radcap_pcie_mixer_linear, TLV_DB_GAIN_MUTE, 600);

static const struct snd_kcontrol_new radcap_pcie_mixer_new = {
	.iface	= SNDRV_CTL_ELEM_IFACE_MIXER,
	.access	= (SNDRV_CTL_ELEM_ACCESS_READWRITE | SNDRV_CTL_ELEM_ACCESS_TLV_READ),
	.info	= radcap_pcie_mixer_info,
	.get	= radcap_pcie_mixer_get,
	.put	= radcap_pcie_mixer_put,
	.tlv.p	= radcap_pcie_mixer_linear,
};

static int radcap_pcie_mixer_create(struct radcap_pcie_snd *chip, struct snd_card *card)
{
	struct radcap_pcie_dev *dev = chip->dev;
	struct snd_kcontrol_new kcontrol_new = radcap_pcie_mixer_new;
	u32 source;
	int ret;

	strscpy(card->mixername, RADCAP_PCIE_DRV_NAME, sizeof(card->mixername));

	switch (dev->board) {
	case RADCAP_PCIE_AM:
		kcontrol_new.name = "AM Capture Volume";
		break;
	case RADCAP_PCIE_FM:
		kcontrol_new.name = "FM Capture Volume";
		break;
	default:
		kcontrol_new.name = "Radio Capture Volume";
		break;
	}

	for (source = 0; source < dev->nodes; source++) {
		mutex_lock(&chip->snd_mlock);
		chip->mixer_volume[source][0] = 0x8000;
		chip->mixer_volume[source][1] = 0x8000;
		mutex_unlock(&chip->snd_mlock);
		kcontrol_new.index = source + 1;
		kcontrol_new.subdevice = source;
		ret = snd_ctl_add(card, snd_ctl_new1(&kcontrol_new, chip));
		if (ret < 0)
			return ret;
	}
	return 0;
}

static int radcap_pcie_snd_card_create(struct radcap_pcie_dev *dev)
{
	struct snd_card *card;
	struct radcap_pcie_snd *chip;
	int ret;

	ret = snd_card_new(&dev->pdev->dev, SNDRV_DEFAULT_IDX1, SNDRV_DEFAULT_STR1, THIS_MODULE,
			   sizeof(*chip), &card);
	if (ret < 0)
		return ret;

	chip = card->private_data;
	chip->dev = dev;
	dev->snd_chip = chip;

	strscpy(card->driver, RADCAP_PCIE_DRV_NAME, sizeof(card->driver));
	snprintf(card->shortname, sizeof(card->shortname), "%s Radcap", dev->card.band);
	snprintf(card->longname, sizeof(card->longname), "%s %s %u irq %u id %llx",
		 card->driver, dev->card.band, dev->card.nodes, dev->pdev->irq, dev->fpga_id);

	mutex_init(&chip->snd_mlock);

	ret = radcap_pcie_pcm_create(chip, card);
	if (ret < 0)
		goto free_snd;

	ret = radcap_pcie_mixer_create(chip, card);
	if (ret < 0)
		goto free_snd;

	ret = snd_card_register(card);
	if (ret < 0)
		goto free_snd;

	dev->snd_card = card;
	dev->snd_ready = true;
	return 0;

free_snd:
	snd_card_free(card);
	dev->snd_chip = NULL;
	return ret;
}

static void radcap_pcie_snd_card_free(struct radcap_pcie_dev *dev)
{
	dev->snd_ready = false;
	dev->snd_chip = NULL;

	if (dev->snd_card) {
		snd_card_free(dev->snd_card);
		dev->snd_card = NULL;
	}
}

static __always_inline void radcap_pcie_fm_audmode_set(struct radcap_pcie_dev *dev)
{
	/* Set all tuners to Mono in dual tuner mode regardless of fm_audmode_mask */
	if (dev->rctrl.dual)
		radcap_pcie_iowrite(dev, dev->card.reg[9], dev->card.val[8]);
	else
		radcap_pcie_iowrite(dev, dev->card.reg[9], dev->rctrl.fm_audmode_mask);
}

static void radcap_pcie_freq_set(struct radcap_pcie_dev *dev, u32 tuner)
{
	s32 freq = (s32)dev->rctrl.freq[tuner];
	u32 val;

	if (dev->error)
		return;

	switch (dev->board) {
	case RADCAP_PCIE_AM:
		freq = clamp_t(s32, freq, radcap_pcie_tuners[RADCAP_TUNER_AM].rangelow,
					  radcap_pcie_tuners[RADCAP_TUNER_AM].rangehigh);
		dev->rctrl.freq[tuner] = (u32)freq;
		val = (tuner << 16) | (u32)radcap_pcie_am_freq(freq / 16);
		radcap_pcie_iowrite(dev, dev->card.reg[6], val);
		break;
	case RADCAP_PCIE_FM:
		freq = clamp_t(s32, freq, radcap_pcie_tuners[RADCAP_TUNER_FM].rangelow,
					  radcap_pcie_tuners[RADCAP_TUNER_FM].rangehigh);
		dev->rctrl.freq[tuner] = (u32)freq;
		val = (tuner << 16) | (u32)radcap_pcie_fm_freq(freq / 16);
		radcap_pcie_iowrite(dev, dev->card.reg[6], val);
		break;
	}
}

/* Last config step */
static void radcap_pcie_demod_set(struct radcap_pcie_dev *dev, bool status)
{
	u32 fm_bw, val = 0;

	if (dev->error)
		return;

	if (!status)
		goto demod_set;

	switch (dev->board) {
	case RADCAP_PCIE_AM:
		val |= dev->rctrl.narrow ? dev->card.val[3] : dev->card.val[2];
		val |= dev->rctrl.dual ? dev->card.val[5] : dev->card.val[4];
		break;
	case RADCAP_PCIE_FM:
		fm_bw = dev->rctrl.narrow ? dev->card.val[3] : dev->card.val[2];
		radcap_pcie_fm_audmode_set(dev);
		radcap_pcie_iowrite(dev, dev->card.reg[10], fm_bw);
		val |= dev->rctrl.dual ? dev->card.val[5] : dev->card.val[4];
		val |= (dev->rctrl.fm_deemph == V4L2_DEEMPHASIS_50_uS) ?
			dev->card.val[6] : dev->card.val[7];
		break;
	}

demod_set:
	mb(); /* Enforce ordering during resume */
	radcap_pcie_iowrite(dev, dev->card.reg[5], val);
}

static int radcap_pcie_querycap(struct file *file, void __always_unused *priv,
				struct v4l2_capability *cap)
{
	struct radcap_pcie_dev *dev = video_drvdata(file);

	snprintf(cap->bus_info, sizeof(cap->bus_info), "PCI:%s", pci_name(dev->pdev));
	snprintf(cap->card, sizeof(cap->card), RADCAP_PCIE_DRV_NAME " %s", dev->card.band);
	strscpy(cap->driver, KBUILD_MODNAME, sizeof(cap->driver));
	return 0;
}

static int radcap_pcie_g_tuner(struct file *file, void __always_unused *priv, struct v4l2_tuner *t)
{
	struct radcap_pcie_dev *dev = video_drvdata(file);
	u32 tuner = t->index;

	if (tuner >= dev->nodes)
		return -EINVAL;

	switch (dev->board) {
	case RADCAP_PCIE_AM:
		*t = radcap_pcie_tuners[RADCAP_TUNER_AM];
		t->index = tuner;
		if (dev->error)
			break;

		t->signal = (s32)min_t(u32, radcap_pcie_ioread(dev,
					    dev->card.reg[8] + tuner) * 15, 0xffff);
		break;
	case RADCAP_PCIE_FM:
		*t = radcap_pcie_tuners[RADCAP_TUNER_FM];
		t->index = tuner;
		if (dev->error)
			break;

		t->signal = (s32)min_t(u32, ioread16((int16_t __iomem *)(dev->ctrl_radio +
					    dev->card.reg[8]) + tuner) * 30, 0xffff);
		t->rxsubchans = (ioread16((int16_t __iomem *)(dev->ctrl_radio +
					 dev->card.reg[11]) + tuner) < 0x0100) ?
					 V4L2_TUNER_SUB_MONO : V4L2_TUNER_SUB_STEREO;
		if (dev->rctrl.fm_audmode_set_mask & BIT(tuner)) /* Restore mode */
			t->audmode = (dev->rctrl.fm_audmode_mask & BIT(tuner)) ?
				     V4L2_TUNER_MODE_MONO : V4L2_TUNER_MODE_STEREO;
		break;
	}
	snprintf(t->name, sizeof(t->name), "tuner %u", t->index);
	return 0;
}

static int radcap_pcie_s_tuner(struct file *file, void __always_unused *priv,
			       const struct v4l2_tuner *t)
{
	struct radcap_pcie_dev *dev = video_drvdata(file);
	u32 tuner = t->index;

	if (tuner >= dev->nodes)
		return -EINVAL;

	if (dev->error)
		return dev->error;

	if (dev->board != RADCAP_PCIE_FM)
		return 0;

	if (t->audmode == V4L2_TUNER_MODE_MONO)
		dev->rctrl.fm_audmode_mask |= BIT(tuner);
	else
		dev->rctrl.fm_audmode_mask &= ~(BIT(tuner));
	dev->rctrl.fm_audmode_set_mask |= BIT(tuner); /* Mark as changed */
	radcap_pcie_fm_audmode_set(dev);
	return 0;
}

static int radcap_pcie_g_frequency(struct file *file, void __always_unused *priv,
				   struct v4l2_frequency *f)
{
	struct radcap_pcie_dev *dev = video_drvdata(file);
	u32 tuner = f->tuner;

	if (tuner >= dev->nodes)
		return -EINVAL;

	f->frequency = dev->rctrl.freq[tuner];
	return 0;
}

static int radcap_pcie_s_frequency(struct file *file, void __always_unused *priv,
				   const struct v4l2_frequency *f)
{
	struct radcap_pcie_dev *dev = video_drvdata(file);
	u32 tuner = f->tuner;

	if (tuner >= dev->nodes)
		return -EINVAL;

	if (dev->error)
		return dev->error;

	dev->rctrl.freq[tuner] = f->frequency;
	radcap_pcie_freq_set(dev, tuner);
	return 0;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int radcap_pcie_g_register(struct file *file, void __always_unused *priv,
				  struct v4l2_dbg_register *reg)
{
	struct radcap_pcie_dev *dev = video_drvdata(file);

	reg->size = sizeof(u32);
	reg->val = (__u64)radcap_pcie_ioread(dev, lower_32_bits(reg->reg));
	return 0;
}

static int radcap_pcie_s_register(struct file *file, void __always_unused *priv,
				  const struct v4l2_dbg_register *reg)
{
	struct radcap_pcie_dev *dev = video_drvdata(file);

	radcap_pcie_iowrite(dev, lower_32_bits(reg->reg), lower_32_bits(reg->val));
	return 0;
}
#endif

static const struct v4l2_file_operations radcap_pcie_fops = {
	.owner		= THIS_MODULE,
	.open		= v4l2_fh_open,
	.release	= v4l2_fh_release,
	.poll		= v4l2_ctrl_poll,
	.unlocked_ioctl	= video_ioctl2,
};

static const struct v4l2_ioctl_ops radcap_pcie_ioctl_ops = {
	.vidioc_querycap		= radcap_pcie_querycap,
	.vidioc_g_tuner			= radcap_pcie_g_tuner,
	.vidioc_s_tuner			= radcap_pcie_s_tuner,
	.vidioc_g_frequency		= radcap_pcie_g_frequency,
	.vidioc_s_frequency		= radcap_pcie_s_frequency,
	.vidioc_log_status		= v4l2_ctrl_log_status,
	.vidioc_subscribe_event		= v4l2_ctrl_subscribe_event,
	.vidioc_unsubscribe_event	= v4l2_event_unsubscribe,
#ifdef CONFIG_VIDEO_ADV_DEBUG
	.vidioc_g_register		= radcap_pcie_g_register,
	.vidioc_s_register		= radcap_pcie_s_register,
#endif
};

static const struct video_device radcap_pcie_rdev = {
	.device_caps	= (V4L2_CAP_RADIO | V4L2_CAP_TUNER),
	.fops		= &radcap_pcie_fops,
	.ioctl_ops	= &radcap_pcie_ioctl_ops,
	.name		= RADCAP_PCIE_DRV_NAME,
	.release	= video_device_release_empty,
};

static int radcap_pcie_s_ctrl(struct v4l2_ctrl *ctrl)
{
	struct radcap_pcie_dev *dev = container_of(ctrl->handler, struct radcap_pcie_dev,
						   ctrl_handler);

	if (dev->error)
		return dev->error;

	switch (ctrl->id) {
	case V4L2_CID_TUNE_DEEMPHASIS:
		if (dev->board != RADCAP_PCIE_FM)
			return -EINVAL;
		dev->rctrl.fm_deemph = ctrl->val;
		break;
	case V4L2_CID_RADCAP_PCIE_NARROW:
		dev->rctrl.narrow = !!ctrl->val;
		break;
	case V4L2_CID_RADCAP_PCIE_DUAL:
		if (dev->nodes % 2)
			return -EINVAL;
		dev->rctrl.dual = !!ctrl->val;
		break;
	default:
		return -EINVAL;
	}
	radcap_pcie_demod_set(dev, true);
	return 0;
}

static const struct v4l2_ctrl_ops radcap_pcie_ctrl_ops = {
	.s_ctrl	= radcap_pcie_s_ctrl,
};

static const struct v4l2_ctrl_config radcap_pcie_ctrl_narrow = {
	.ops	= &radcap_pcie_ctrl_ops,
	.id	= V4L2_CID_RADCAP_PCIE_NARROW,
	.name	= "Narrow Audio",
	.type	= V4L2_CTRL_TYPE_BOOLEAN,
	.max	= 1,
	.step	= 1,
};

static const struct v4l2_ctrl_config radcap_pcie_ctrl_dual = {
	.ops	= &radcap_pcie_ctrl_ops,
	.id	= V4L2_CID_RADCAP_PCIE_DUAL,
	.name	= "Dual Tuner",
	.type	= V4L2_CTRL_TYPE_BOOLEAN,
	.max	= 1,
	.step	= 1,
};

static void radcap_pcie_hw_write_key(struct radcap_pcie_dev *dev)
{
	u32 k;

	/* If fpga_id matches then write fpga_key to enable more nodes */
	for (k = 0; k < key_count; k++) {
		u32 fpga_key = 0;
		u64 fpga_id = 0;
		int read = sscanf(key[k], "%15llx:%u", &fpga_id, &fpga_key);

		if ((read == 2) && (fpga_id == dev->fpga_id))
			radcap_pcie_iowrite(dev, dev->card.reg[3], fpga_key);
	}
}

static int radcap_pcie_hw_setup(struct radcap_pcie_dev *dev)
{
	if (readx_poll_timeout(lo_hi_readq, dev->ctrl_radio + dev->card.reg[4],
			       dev->fpga_id, dev->fpga_id, 10, 1000))
		return -ETIMEDOUT;

	if (upper_32_bits(dev->fpga_id) == 0xffffffff)
		return -ENODEV;

	if (key_count) {
		radcap_pcie_hw_write_key(dev); /* Write the key */
		usleep_range(50, 1000);
	}

	dev->card.nodes = radcap_pcie_ioread(dev, dev->card.reg[2]) & 0xffff;
	if ((dev->card.nodes < RADCAP_PCIE_MIN_NODES) || (dev->card.nodes > RADCAP_PCIE_MAX_NODES))
		return -ENODEV;
	dev->nodes = dev->card.nodes;
	if ((nodes[dev->nr] > 0) && (nodes[dev->nr] <= dev->nodes))
		dev->nodes = nodes[dev->nr]; /* Set nodes */
	return 0;
}

static void radcap_pcie_hw_reset(struct radcap_pcie_dev *dev)
{
	u32 node;

	for (node = 0; node < dev->nodes; node++) {
		radcap_pcie_iowrite(dev, dev->card.reg[0] + node, dev->card.val[0]);
		radcap_pcie_freq_set(dev, node);
	}
}

static irqreturn_t radcap_pcie_irq(int __always_unused irq, void *dev_id)
{
	struct radcap_pcie_dev *dev = dev_id;
	u32 status;

	spin_lock(&dev->slock);
	if (dev->error)
		goto irq_none;

	status = radcap_pcie_ioread(dev, dev->card.reg[1]);
	if (!status) {
		goto irq_none;
	} else if (status == 0xffffffff) {
		dev->error = -ENODEV; /* Device disconnected */
		goto irq_none;
	}

	if ((status & BIT(0)) && dev->snd_ready && dev->snd_chip) {
		struct radcap_pcie_snd *chip = dev->snd_chip;
		u32 source;

		for (source = 0; source < dev->nodes; source++) {
			if ((chip->pcm_substream_mask & BIT(source)) &&
			     chip->pcm_substream[source]) {
				spin_unlock(&dev->slock);
				snd_pcm_period_elapsed(chip->pcm_substream[source]);
				spin_lock(&dev->slock);
			}
		}
	}
	spin_unlock(&dev->slock);
	return IRQ_HANDLED;

irq_none:
	spin_unlock(&dev->slock);
	return IRQ_NONE;
}

static int radcap_pcie_probe(struct pci_dev *pdev, const struct pci_device_id *pci_id)
{
	struct radcap_pcie_dev *dev;
	struct v4l2_device *v4l2_dev;
	struct v4l2_ctrl_handler *hdl;
	int ret;

	if (radcap_pcie_devcount >= RADCAP_PCIE_MAX_CARDS)
		return -ENODEV;

	ret = pci_enable_device(pdev);
	if (ret)
		return ret;

	pci_set_master(pdev);

	if (dma_set_mask(&pdev->dev, DMA_BIT_MASK(32))) {
		dev_err(&pdev->dev, "no suitable DMA support available\n");
		ret = -EFAULT;
		goto disable_device;
	}

	dev = devm_kzalloc(&pdev->dev, sizeof(*dev), GFP_KERNEL);
	if (!dev) {
		ret = -ENOMEM;
		dev_err(&pdev->dev, "can't allocate memory\n");
		goto disable_device;
	}

	dev->board = pci_id->driver_data;
	dev->card = radcap_pcie_cards[dev->board];
	dev->nr = radcap_pcie_devcount;
	dev->pdev = pdev;
	snprintf(dev->name, sizeof(dev->name), RADCAP_PCIE_DRV_NAME " [%d] [%s]",
		 dev->nr, dev->card.band);

	ret = pci_request_regions(pdev, KBUILD_MODNAME);
	if (ret) {
		radcap_pcie_pr_err("can't request memory regions\n");
		goto disable_device;
	}

	dev->ctrl_radio = pci_ioremap_bar(pdev, 0);
	if (!dev->ctrl_radio) {
		ret = -ENOMEM;
		radcap_pcie_pr_err("can't ioremap Radio Control BAR\n");
		goto free_pci_regions;
	}

	dev->ctrl_pcm = pci_ioremap_bar(pdev, 2);
	if (!dev->ctrl_pcm) {
		ret = -ENOMEM;
		radcap_pcie_pr_err("can't ioremap PCM Control BAR\n");
		goto free_ctrl_radio;
	}

	spin_lock_init(&dev->slock);

	/* Default values */
	dev->rctrl.fm_deemph = V4L2_DEEMPHASIS_75_uS;
	dev->rctrl.narrow = false;
	dev->rctrl.dual = false;

	ret = radcap_pcie_hw_setup(dev); /* Get nodes */
	if (ret) {
		radcap_pcie_pr_err("error %d when detecting nodes\n", ret);
		goto free_ctrl_pcm;
	}

	radcap_pcie_hw_reset(dev);
	radcap_pcie_demod_set(dev, true);

	ret = request_irq(pdev->irq, radcap_pcie_irq, IRQF_SHARED, KBUILD_MODNAME, dev);
	if (ret) {
		radcap_pcie_pr_err("failed to request IRQ\n");
		goto free_ctrl_pcm;
	}

	dev->irq = pdev->irq;
	synchronize_irq(dev->irq);

	ret = radcap_pcie_snd_card_create(dev); /* Create sound card */
	if (ret)
		goto free_irq;

	/* Initialize the top-level structure */
	mutex_init(&dev->rdev_mlock);
	v4l2_dev = &dev->v4l2_dev;
	ret = v4l2_device_register(&pdev->dev, v4l2_dev);
	if (ret) {
		v4l2_err(v4l2_dev, "can't register V4L2 device\n");
		goto free_snd_card;
	}

	/* Add the controls */
	hdl = &dev->ctrl_handler;
	v4l2_ctrl_handler_init(hdl, 3);
	v4l2_ctrl_new_custom(hdl, &radcap_pcie_ctrl_narrow, NULL);

	/* Dual Tuner mode, 2 Tuners per ALSA Subdevice */
	if (!(dev->nodes % 2))
		v4l2_ctrl_new_custom(hdl, &radcap_pcie_ctrl_dual, NULL);
	if (dev->board == RADCAP_PCIE_FM)
		v4l2_ctrl_new_std_menu(hdl, &radcap_pcie_ctrl_ops, V4L2_CID_TUNE_DEEMPHASIS,
				       V4L2_DEEMPHASIS_75_uS, 1, V4L2_DEEMPHASIS_75_uS);
	v4l2_dev->ctrl_handler = hdl;
	if (hdl->error) {
		ret = hdl->error;
		v4l2_err(v4l2_dev, "can't register V4L2 controls\n");
		goto free_v4l2;
	}

	/* Initialize the video_device structure */
	strscpy(v4l2_dev->name, dev->name, sizeof(v4l2_dev->name));
	dev->rdev = radcap_pcie_rdev;
	dev->rdev.ctrl_handler = &dev->ctrl_handler;
	dev->rdev.lock = &dev->rdev_mlock;
	dev->rdev.v4l2_dev = v4l2_dev;
	video_set_drvdata(&dev->rdev, dev);

	ret = video_register_device(&dev->rdev, VFL_TYPE_RADIO, -1);
	if (ret)
		goto free_v4l2;

	radcap_pcie_pr_info("Card ID: %llx, %u %s tuners detected\n",
			    dev->fpga_id, dev->card.nodes, dev->card.band);
	radcap_pcie_pr_info("IRQ: %u, Radio Control: %pR, PCM Control: %pR\n",
			    dev->pdev->irq, &dev->pdev->resource[0], &dev->pdev->resource[2]);
	radcap_pcie_pr_info("registered as %s, %u %s tuners enabled\n",
			    video_device_node_name(&dev->rdev), dev->nodes, dev->card.band);
	radcap_pcie_devcount++;
	return 0;

free_v4l2:
	v4l2_ctrl_handler_free(hdl);
	v4l2_device_unregister(v4l2_dev);
free_snd_card:
	radcap_pcie_snd_card_free(dev);
free_irq:
	free_irq(dev->irq, dev);
free_ctrl_pcm:
	iounmap(dev->ctrl_pcm);
free_ctrl_radio:
	iounmap(dev->ctrl_radio);
free_pci_regions:
	pci_release_regions(pdev);
disable_device:
	pci_disable_device(pdev);
	return ret;
}

static void radcap_pcie_remove(struct pci_dev *pdev)
{
	struct v4l2_device *v4l2_dev = pci_get_drvdata(pdev);
	struct radcap_pcie_dev *dev = container_of(v4l2_dev, struct radcap_pcie_dev, v4l2_dev);

	radcap_pcie_demod_set(dev, false); /* Shutdown card */
	usleep_range(50, 1000);
	radcap_pcie_pr_info("removing %s\n", video_device_node_name(&dev->rdev));
	radcap_pcie_devcount--;
	video_unregister_device(&dev->rdev);
	v4l2_ctrl_handler_free(&dev->ctrl_handler);
	v4l2_device_unregister(&dev->v4l2_dev);
	radcap_pcie_snd_card_free(dev); /* Free sound card */
	free_irq(dev->irq, dev);
	iounmap(dev->ctrl_pcm);
	iounmap(dev->ctrl_radio);
	pci_release_regions(pdev);
	pci_disable_device(pdev);
}

static int __maybe_unused radcap_pcie_suspend(struct device *dev_d)
{
	struct v4l2_device *v4l2_dev = dev_get_drvdata(dev_d);
	struct radcap_pcie_dev *dev = container_of(v4l2_dev, struct radcap_pcie_dev, v4l2_dev);

	if (dev->error)
		return dev->error;

	radcap_pcie_demod_set(dev, false);
	return 0;
}

static int __maybe_unused radcap_pcie_resume(struct device *dev_d)
{
	struct v4l2_device *v4l2_dev = dev_get_drvdata(dev_d);
	struct radcap_pcie_dev *dev = container_of(v4l2_dev, struct radcap_pcie_dev, v4l2_dev);

	if (dev->error)
		return dev->error;

	if (key_count) {
		radcap_pcie_hw_write_key(dev);
		usleep_range(50, 1000);
	}
	radcap_pcie_hw_reset(dev);
	radcap_pcie_demod_set(dev, true);
	return 0;
}

static const struct pci_device_id radcap_pcie_tbl[] = {
	{ PCI_DEVICE(0x1bc9, 0x3000), .driver_data = RADCAP_PCIE_AM },
	{ PCI_DEVICE(0x1bc9, 0x3101), .driver_data = RADCAP_PCIE_FM },
	{ }
};
MODULE_DEVICE_TABLE(pci, radcap_pcie_tbl);

static SIMPLE_DEV_PM_OPS(radcap_pcie_pm_ops, radcap_pcie_suspend, radcap_pcie_resume);

static struct pci_driver radcap_pcie_driver = {
	.name		= KBUILD_MODNAME,
	.id_table	= radcap_pcie_tbl,
	.probe		= radcap_pcie_probe,
	.remove		= radcap_pcie_remove,
	.driver.pm	= &radcap_pcie_pm_ops,
};

module_pci_driver(radcap_pcie_driver);
